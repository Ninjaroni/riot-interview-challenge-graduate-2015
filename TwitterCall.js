/**
 * Created by Simon on 15/07/2015.
 */

var cb = new Codebird; //connecting to my Twitter app with my super secret keys
cb.setConsumerKey("VpsFmUq64eaVg7dtxG1whhy4Q", "rQIbmg6FJtSqgVpYloVG2OgahXxeDEa6VJKLvMQVxUHI9JSTfx");
cb.setToken("146842730-xsYAYns6NqaVzey1LCluz7SWvOE9GC0bB4osYhHp", "rOBsP31V1JPHmFMjduVB5G4XYN2aF3pzs6U93XiRfLWC4");

var params = {
    //q = the query that's being made
    //count = the amount of tweet that are returned
    //en = only english tweets
    q: "#cwwin OR #elwin OR #fncwin OR #gmbwin OR #giawin OR #h2kwin OR #ogwin OR #rocwin OR #skwin OR #uolwin",
    count : "100",
    lang : "en"
};

cb.__call(
    "search/tweets", //searching tweets
    params,
    function (reply) {

        var cwCount = 0, elCount = 0, fncCount = 0, gmbCount = 0, giaCount = 0, h2kCount = 0, ogCount = 0, rocCount = 0
            ,skCount = 0, uolCount = 0; //counter for displaying percentages

        document.getElementById("loading").style.visibility = "visible"; //"obtaining data" message visible

        console.log(reply);

        for(var i = 0; i < reply.statuses.length; i++) //for the length of the amount of tweets
        {
            var cwdiv = document.getElementById("cw"),eldiv = document.getElementById("el"), //addressing the team divs
                fncdiv = document.getElementById("fnc"),gmbdiv = document.getElementById("gmb"),
                giadiv = document.getElementById("gia"),h2kdiv = document.getElementById("h2k"),
                ogdiv = document.getElementById("og"),rocdiv = document.getElementById("roc"),
                skdiv = document.getElementById("sk"),uoldiv = document.getElementById("uol");

            var point=document.createElement("img");//creating the visual points
            point.setAttribute("src","graphics/point.png"); //setting the point attributes
            point.setAttribute("width","0.5%");
            point.setAttribute("height","0.4%");

            if(reply.statuses[i].text.includes("cwwin") || reply.statuses[i].text.includes("CWWIN"))
            {
                cwCount += 1;
                cwdiv.appendChild(point); //stick a point onto a team if the text includes the string
            }
            else if(reply.statuses[i].text.includes("elwin") || reply.statuses[i].text.includes("ELWIN"))
            {
                elCount += 1;
                eldiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("fncwin") || reply.statuses[i].text.includes("FNCWIN"))
            {
                fncCount += 1;
                fncdiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("gmbwin") || reply.statuses[i].text.includes("GMBWIN"))
            {
                gmbCount += 1;
                gmbdiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("giawin") || reply.statuses[i].text.includes("GIAWIN"))
            {
                giaCount += 1;
                giadiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("h2kwin") || reply.statuses[i].text.includes("H2KWIN"))
            {
                h2kCount += 1;
                h2kdiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("ogwin") || reply.statuses[i].text.includes("OGWIN"))
            {
                ogCount += 1;
                ogdiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("rocwin") || reply.statuses[i].text.includes("ROCWIN"))
            {
                rocCount += 1;
                rocdiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("skwin") || reply.statuses[i].text.includes("SKWIN"))
            {
                skCount += 1;
                skdiv.appendChild(point);
            }
            else if(reply.statuses[i].text.includes("uolwin") || reply.statuses[i].text.includes("UOLWIN"))
            {
                uolCount += 1;
                uoldiv.appendChild(point);
            }
        }

        //creating the percentages on the right side of the screen
        var percentages = document.getElementById("percentages"),cwtext=document.createElement("p"),
            eltext=document.createElement("p"),fnctext=document.createElement("p"),gmbtext=document.createElement("p"),
            giatext=document.createElement("p"),h2ktext=document.createElement("p"),ogtext=document.createElement("p"),
            roctext=document.createElement("p"),sktext=document.createElement("p"),uoltext=document.createElement("p");

        //just a bit of text formatting to make it look nice
        cwtext.innerHTML = "COPENHAGEN WOLVES" +" - "+ cwCount + "%";
        eltext.innerHTML = "ELEMENTS" +" - "+ elCount + "%";
        fnctext.innerHTML = "FNATIC" +" - "+ fncCount + "%";
        gmbtext.innerHTML = "GAMBIT" +" - "+ gmbCount + "%";
        giatext.innerHTML = "GIANTS GAMING" +" - "+ giaCount + "%";
        h2ktext.innerHTML = "H2K" +" - "+ h2kCount + "%";
        ogtext.innerHTML = "ORIGEN" +" - "+ ogCount + "%";
        roctext.innerHTML = "ROCCAT" +" - "+ rocCount + "%";
        sktext.innerHTML = "SK GAMING" +" - "+ skCount + "%";
        uoltext.innerHTML = "UNICORNS OF LOVE" +" - "+ uolCount + "%";

        //appending the text to the holder
        cwdiv.appendChild(cwtext);
        eldiv.appendChild(eltext);
        fncdiv.appendChild(fnctext);
        gmbdiv.appendChild(gmbtext);
        giadiv.appendChild(giatext);
        h2kdiv.appendChild(h2ktext);
        ogdiv.appendChild(ogtext);
        rocdiv.appendChild(roctext);
        skdiv.appendChild(sktext);
        uoldiv.appendChild(uoltext);

        //once the calculations are done, hide the "obtaining data" message
        document.getElementById("loading").style.visibility = "hidden";
    }
);


